import express from 'express';
export const router = express.Router()
import passport from 'passport';
import passportlocal from 'passport-local'
import { user } from '../models/user';
import passportGoogle from 'passport-google-oauth20'

const GoogleStrategy = passportGoogle.Strategy
const LocalStrategy = passportlocal.Strategy


// ----RUTAS-----------------------------------

router.get('/', (req, res, next) => {
    res.render('index')
})

router.get('/login', (req, res, next) => {
    res.render('login')
}) 

router.post('/login',passport.authenticate('local-login',{
    successRedirect: '/profile',
    failureRedirect:'/login',
    passReqToCallback: true
}))

router.get('/singup', (req, res, next) => {
    res.render('singup')
})

router.post('/singup', passport.authenticate('local',{
    successRedirect: '/profile',
    failureRedirect:'/singup',
    passReqToCallback: true
}))

router.get('/profile',isAuthenticated, (req, res, next) => {
    res.render('profile')
})

router.get('/logout', (req, res, next) => {
    //req.session.destroy();borra las credenciales de inicio de sesion
    req.logout(); 
    //res.clearCookie(sessionCookieName);borra datos de la cookie
    res.redirect('/'); 
})

router.get('/google', passport.authenticate('google-token'), () => {
    
});


/** para multiples rutas, desde donde se coloque de hay para abajo las rutas se evaluan 
 router.use((req, res, next) => {
    isAuthenticated(req, res, next)
    next()
})
 */

function isAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }else{
        res.redirect('/login')
    }
}


//---PASSPORT CONFIG------------------------------

passport.use('local', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, async (req, email, password, done) => {
    
        if (req.body.password !== req.body.password2) {
            return done(null, false, req.flash('error_msg','Las contraseñas deben ser correctas'))
        }
        if (req.body.password.length < 6) {
            return done(null, false, req.flash('error_msg','la contraseña debe tener minimo 6 caracteres'))
        }
        try {
            const userfind = await user.findOne({email:email})
            if(userfind){
                return done(null, false, req.flash('error_msg','The email is already exist'))
            }else{
                const newUser = new user()
                newUser.nameuser = req.body.nameuser,
                newUser.email = email,
                newUser.password = newUser.ecryptPassword(password)
                await newUser.save()
                console.log(req.body);
                return done(null, newUser, req.flash('error_msg','The user has registered '))
            }
        } catch (error) {
            console.log(error);
        }   
    }) 
)

passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },async (req, email, password, done) => {
        try {
            const userFind = await user.findOne({email: email})
            if(!userFind){
                return done(null, false, req.flash('error_msg','The user no found'))
            }
            if (!userFind.comparePassword(password)) {
                return done(null, false, req.flash('error_msg','Incorrrect Password'))
            }
            done(null, userFind)
        } catch (error) {
            console.log(error);            
        }        
    })
)
////------------------------SERIALIZER AUTH-----------------

passport.serializeUser((user, done) => {// serializa los datos para guardarlo en una session
    done(null, user.id)
})

passport.deserializeUser(async (id, done) => {
    const User = await user.findById(id)
    done(null, User)
})

//---PASSPORT GOOGLE AUTH-CONFIG------------------------------

router.get('/auth/google', passport.authenticate('google', { scope: ['profile'] }));

router.get('/auth/google/callback', passport.authenticate('google', { 
  successRedirect: '/profile', 
  failureRedirect: '/login'
}));

passport.use(new GoogleStrategy({
      clientID: '406355284163-5026pjjppm4t39e7l0ivt0c4hr6vhi23.apps.googleusercontent.com',
      clientSecret: 'XSWUIoP5Ghd0Ihy65IKOCdUe',
      callbackURL: "http://localhost:8000/auth/google/callback"
    },
async function(accessToken, refreshToken, profile, done) {
      try {
        const userFind = await user.findOne({nameuser: profile.displayName})
        if(userFind){
            return done(null, userFind)
        }else{
            const newUser = new user()
            newUser.nameuser = profile.displayName,
            newUser.email = profile.id,
            newUser.password = accessToken
            newUser.photos = profile._json.image.url
            await newUser.save()
            return done(null, newUser)
        }
      } catch (error) {
        done(error)
      }
    }
));