import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const schema = new mongoose.Schema({
    nameuser: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    photos: {
        type: String,
    },
    date: {
        type: String,
        default: Date.now
    }
})

schema.methods.ecryptPassword = (password) => {
    var salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt) //return contraseña cifrada
}

schema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password, this.password)// return comparacion (TRUE OR FALSE)
}

export const user = mongoose.model('users', schema)


