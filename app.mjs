import express from 'express';
import engine from 'ejs-mate';
import { router as index } from './routes/index';
import morgan from 'morgan';
import mongoose from 'mongoose';
import key from './config/keys';
import passport from 'passport';
import session from 'express-session';
import flash from 'connect-flash';
import passportGoggle from './models/passportConfig';

import sessionFileStore from 'session-file-store';
const FileStore = sessionFileStore(session); 

const app = express();
const port = process.env.NAME_PORT || 5000;

//settings
app.set('view engine', 'ejs')
app.engine('ejs', engine)

//---DB
mongoose.connect(key, { useNewUrlParser: true })
    .then(() => console.log('conectado a la DB MONGO...ATLAS'))
    .catch(err => console.log(err))


//middlewares
app.use(morgan('dev'))
app.use(express.urlencoded({ extended: false }))
app.use(session({
    //store: new FileStore({ path: "sessions" }),
    secret: 'mysecrer session',
    resave: true,
    saveUninitialized: false,
    cookie: { maxAge: 3600000,secure: false, httpOnly: true }
})) 

app.use(passport.initialize())
app.use(passport.session())

//----FLASH
app.use(flash())

//---GLOBAL VARS
app.use((req, res, next) => {
    res.locals.error_msg = req.flash('error_msg')
    res.locals.user = req.user
    next()
})

// routes
app.use('/', index)

//port
app.listen(port, () => {
    console.log(`Corriendo servidor en el puerto ${port}`);
})
